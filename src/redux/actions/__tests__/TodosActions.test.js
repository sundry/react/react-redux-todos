import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../TodosActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Todos Actions', () => {
  it('should create an action to add a todo', () => {
    const text = 'First Todo';
    const expectedActions = [
      {
        id: 0,
        type: types.ADD_TODO,
        text,
      },
    ];
    const store = mockStore([]);
    store.dispatch(actions.addTodo(text));
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('should create an action to toggle a todo', () => {
    const id = 0;
    const expectedActions = [
      {
        type: types.TOGGLE_TODO,
        id,
      },
    ];
    const store = mockStore([]);
    store.dispatch(actions.toggleTodo(id));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
