import React, { Component } from 'react';
import './app.css';
import Footer from '../../components/Footer/Footer';
import AddTodo from '../../containers/AddTodo/AddTodo';
import VisibilityTodoList from '../../containers/VisibilityTodoList/VisibilityTodoList';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <AddTodo />
        <VisibilityTodoList />
        <Footer />
      </div>
    );
  }
}
